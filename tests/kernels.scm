;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019, 2021 Inria
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (tests kernels)
  #:use-module (jupyter kernels)
  #:use-module (jupyter messages)
  #:use-module (simple-zmq)
  #:use-module (json parser)
  #:use-module (json builder)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 match))

(define %context
  (zmq-create-context))

(define %python3-specs
  (false-if-exception (find-kernel-specs "python3")))

(define %kernel-key "secretkey")

(define %kernel #f)

(test-begin "kernels")

;; These tests require the "python3" kernel provided by ipykernel.
(unless %python3-specs (test-skip 1))

(test-assert "run-kernel python3"
  (let ((kernel (run-kernel %context %python3-specs %kernel-key)))
    (set! %kernel kernel)
    (and (kernel? (pk 'kernel kernel))
         (begin
           (kill (kernel-pid kernel) 0)

           ;; FIXME: For some reason, we don't always get a "starting" status
           ;; message.  Thus, don't fail if we don't receive it.
           (let ((greetings (read-message %kernel 5000)))
             (or (not greetings)
                 (list (message-type greetings)
                       (kernel-status-execution-state
                        (json->kernel-status
                         (message-content greetings))))))))))

(unless %kernel (test-skip 1))
(test-equal "kernel_info_request"
  "python"
  (let ((request (message (header "kernel_info_request" "luser" "12345")
                          "{}")))
    (send-message %kernel request)
    (let* ((replies (unfold (cut > <> 2)
                            (lambda (_)
                              (read-message %kernel 10000))
                            1+ 0)))
      (define (type-predicate type)
        (lambda (message)
          (string=? (message-type message) type)))

      (and (every message? (pk 'replies replies))

           (match (filter (type-predicate "status") replies)
             ((busy idle)
              ;; We got two message: busy *then* idle.
              (and (eq? 'busy
                        (kernel-status-execution-state
                         (json->kernel-status (message-content busy))))
                   (eq? 'idle
                        (kernel-status-execution-state
                         (json->kernel-status (message-content idle))))
                   (equal? (message-parent-header busy)
                           (message-header request))
                   (equal? (message-parent-header idle)
                           (message-header request)))))

           (let ((reply (find (type-predicate "kernel_info_reply")
                              replies)))
             (and (equal? (message-parent-header reply)
                          (message-header request))
                  (let ((reply (json->kernel-info-reply
                                (message-content reply))))
                    (language-info-name
                     (kernel-info-reply-language-info reply)))))))))

(unless %kernel (test-skip 1))
(test-equal "execute_request"
  42
  (let ((request (message (header "execute_request" "luser" "12345")
                          (scm->json-string
                           (execute-request->json
                            (execute-request (code "40 + 2\n")))))))
    (send-message %kernel request)

    ;; As noted at
    ;; <https://jupyter-client.readthedocs.io/en/latest/messaging.html#request-reply>,
    ;; in the request-reply pattern, we first receive on the iopub socket a
    ;; "status" message, then our input is broadcast on iopub, and then we
    ;; get the result.
    ;;
    ;; However, ordering is not guaranteed: since we poll on all these
    ;; sockets, we might get the iopub message before the shell message.
    ;; Thus, this test does not make any assumptions on ordering.
    (let* ((replies (unfold (cut > <> 4)
                            (lambda (_)
                              (read-message %kernel 10000))
                            1+ 0)))
      (define (type-predicate type)
        (lambda (message)
          (string=? (message-type message) type)))

      (and (every message? (pk 'replies replies))

           (match (filter (type-predicate "status") replies)
             ((busy idle)
              ;; We got two message: busy *then* idle.
              (and (eq? 'busy
                        (kernel-status-execution-state
                         (json->kernel-status (message-content busy))))
                   (eq? 'idle
                        (kernel-status-execution-state
                         (json->kernel-status (message-content idle))))
                   (equal? (message-parent-header busy)
                           (message-header request))
                   (equal? (message-parent-header idle)
                           (message-header request)))))

           (let ((input (find (type-predicate "execute_input") replies)))
             (equal? (message-parent-header input) (message-header request)))

           (let ((reply (find (type-predicate "execute_reply") replies)))
             (equal? (message-parent-header reply) (message-header request)))

           (let ((result (find (type-predicate "execute_result") replies)))
             (and (equal? (message-parent-header result)
                          (message-header request))
                  (let* ((content (json-string->scm (message-content result)))
                         (data    (assoc-ref content "data"))
                         (text    (assoc-ref data "text/plain")))
                    (string->number text))))))))

(when %kernel
  (close-kernel %kernel)
  (kill (kernel-pid %kernel) 15))

(test-end "kernels")
