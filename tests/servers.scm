;;; Guix-kernel -- Guix kernel for Jupyter
;;; Copyright (C) 2019, 2021 Inria
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (tests kernels)
  #:use-module (jupyter kernels)
  #:use-module (jupyter messages)
  #:use-module (jupyter servers)
  #:use-module (simple-zmq)
  #:use-module (json parser)
  #:use-module (json builder)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 threads)
  #:use-module (ice-9 match))

(define %client-context
  (zmq-create-context))
(define %server-context
  (zmq-create-context))

(define %kernel-key "secretkey")


(define %first-port
  ;; First TCP port to use.  This is meant to avoid collisions with other
  ;; tests that may be running in parallel.
  1500)

(test-begin "servers")

(test-equal "ping pong, one kernel, shell"
  42
  (let* ((connection client (allocate-connection %client-context
                                                 "tcp" "127.0.0.1"
                                                 %kernel-key
                                                 #:first-port %first-port))
         (server            (connection->kernel connection
                                                #:context %server-context)))
    (define (handle-message kernel kind message state)
      (pk 'handle kernel message)
      (let ((last? (= state 42)))
        (send-message kernel
                      (if last?
                          (reply message "quit" "{}")
                          (reply message "pong" (scm->json-string state))))
        (if last?
            (leave-server-loop state)
            (+ 1 state))))

    (define (client-thunk)
      (let ((request (message (header "ping" "foo" "12345") "{}")))
        (send-message client request)
        (let ((message (read-message client)))
          (if (string=? "quit" (message-type message))
              (close-kernel client)
              (client-thunk)))))

    (call-with-new-thread client-thunk)
    (let ((result (serve-kernels (list server) handle-message 0)))
      (close-kernel server)
      result)))

(test-equal "ping pong, one kernel, shell + control + stdin"
  (make-list 3 1)                ;1 message received on each of the 3 sockets
  (let* ((connection client (allocate-connection %client-context
                                                 "tcp" "127.0.0.1"
                                                 %kernel-key
                                                 #:first-port %first-port))
         (server            (connection->kernel connection
                                                #:context %server-context)))
    (define (handle-message kernel kind message state)
      (send-message kernel (reply (pk 'handle kind message) "pong" "{}")
                    #:kernel-socket kind)
      (let* ((n     (or (assq-ref state kind) 0))
             (state (alist-cons kind (+ n 1)
                                (alist-delete kind state eq?)))
             (total (reduce + 0 (map cdr state))))
        (if (>= total 3)
            (leave-server-loop state)
            state)))

    (define (client-thunk)
      (for-each (lambda (socket)
                  (let ((request (message (header "ping" "foo" "12345") "{}")))
                    (send-message client request
                                  #:kernel-socket socket)
                    (read-message client)))
                (list kernel-shell kernel-standard-input kernel-control))
      (close-kernel client))

    (call-with-new-thread client-thunk)

    ;; The server's state is an alist mapping each kernel socket procedure to
    ;; the number of messages received on that socket.
    (match (serve-kernels (list server) handle-message '())
      ((((? procedure?) . counts) ...)
       (close-kernel server)
       counts))))

(test-equal "ping pong, many clients, shell"
  (make-list 10 77)             ;number of messages received from each kernel

  ;; Start KERNELS clients, each of which sends ROUNDS messages to the
  ;; server, which replies.
  (let* ((kernels 10)                             ;number of kernels
         (rounds  77)                             ;number of messages
         (connections
          clients
          (unzip2
           (map (lambda (i)
                  (define-values (connection client)
                    (allocate-connection %client-context
                                         "tcp" "127.0.0.1"
                                         %kernel-key
                                         #:first-port
                                         (+ %first-port (* i 10))))
                  (list connection client))
                (iota kernels))))
         (servers (map (lambda (connection)
                         (connection->kernel connection
                                             #:context
                                             %server-context))
                       connections)))
    (define (handle-message kernel kind message state)
      (send-message kernel (reply message "pong" "{}"))
      (let* ((n     (or (assq-ref state kernel) 0))
             (state (alist-cons kernel (+ n 1)
                                (alist-delete kernel state eq?)))
             (total (reduce + 0 (map cdr state))))
        (if (>= total (* kernels rounds))
            (leave-server-loop state)
            state)))

    (define (client-proc client)
      (let loop ((n 1))
        (let ((request (message (header "ping" "foo" "12345") "{}")))
          (send-message client request)
          (let ((message (read-message client)))
            (if (< n rounds)
                (loop (+ 1 n))
                (close-kernel client))))))

    (for-each (lambda (client)
                (call-with-new-thread (cut client-proc client)))
              clients)

    ;; The server's state is an alist mapping each kernel to the number of
    ;; messages received from that kernel.  At the end, return the number of
    ;; messages received from each kernel.
    (match (serve-kernels servers handle-message '())
      ((((? kernel?) . counts) ...)
       (for-each close-kernel servers)
       counts))))

(test-equal "ping pong, many clients coming dynamically, shell"
  (make-list 10 77)             ;number of messages received from each kernel

  ;; Start KERNELS clients, each of which sends ROUNDS messages to the
  ;; server, which replies.
  (let* ((kernels 10)                             ;number of clients
         (rounds  77)                             ;number of messages
         (connections
          clients
          (unzip2
           (map (lambda (i)
                  (define-values (connection client)
                    (allocate-connection %client-context
                                         "tcp" "127.0.0.1"
                                         %kernel-key
                                         #:first-port
                                         (+ %first-port (* i 10))))
                  (list connection client))
                (iota kernels))))
         (servers (map (lambda (connection)
                         (connection->kernel connection
                                             #:context
                                             %server-context))
                       connections)))
    (define (increment-counter kernel state)
      (let ((n (or (assq-ref state kernel) 0)))
        (alist-cons kernel (+ n 1)
                    (alist-delete kernel state eq?))))

    (define (handle-message kernel kind message state)
      (send-message kernel (reply message "pong" "{}"))
      (match state
        (((next rest ...) state)
         (let ((state (increment-counter kernel state)))
           (monitor-client next)                  ;add a new client to serve
           (list rest state)))
        ((() state)
         (let* ((state (increment-counter kernel state))
                (total (reduce + 0 (map cdr state))))
           (if (>= total (* kernels rounds))
               (leave-server-loop state)
               (list '()  state))))))

    (define (client-proc client)
      (let loop ((n 1))
        (let ((request (message (header "ping" "foo" "12345") "{}")))
          (send-message client request)
          (let ((message (read-message client)))
            (when (< n rounds)
              (loop (+ 1 n)))))))

    (for-each (lambda (client)
                (call-with-new-thread (cut client-proc client)))
              clients)

    ;; The server's state is an alist mapping each kernel to the number of
    ;; messages received from that kernel, plus the list of additional
    ;; clients to monitor.  At the end, return the number of messages
    ;; received from each kernel.
    (match (serve-kernels (list (first servers)) handle-message
                          (list (cdr servers) '()))
      ((((? kernel?) . counts) ...)
       (for-each close-kernel servers)
       counts))))

(test-equal "ping pong, many clients leaving dynamically, shell"
  10

  ;; Start KERNELS clients, each of which sends 1 message to the server,
  ;; which replies and then "unmonitors" the sender.
  (let* ((kernels 10)                             ;number of kernels
         (connections
          clients
          (unzip2
           (map (lambda (i)
                  (define-values (connection client)
                    (allocate-connection %client-context
                                         "tcp" "127.0.0.1"
                                         %kernel-key
                                         #:first-port
                                         (+ %first-port (* i 10))))
                  (list connection client))
                (iota kernels))))
         (servers (map (lambda (connection)
                         (connection->kernel connection
                                             #:context
                                             %server-context))
                       connections)))
    (define (handle-message kernel kind message count)
      (send-message kernel (reply message "pong" "{}"))
      (unmonitor-client kernel)
      (close-kernel kernel)
      (let ((count (+ count 1)))
        (if (>= count kernels)
            (leave-server-loop count)
            count)))

    (define (client-proc client)
      (let ((request (message (header "ping" "foo" "12345") "{}")))
        (send-message client request)
        (read-message client)
        (close-kernel client)))

    (for-each (lambda (client)
                (call-with-new-thread (cut client-proc client)))
              clients)

    ;; The server state is a message counter.
    (serve-kernels servers handle-message 0)))

(test-equal "heartbeat, one kernel"
  "success"
  ;; Ensure that heartbeat messages are automatically echoed back.
  (let* ((connection client (allocate-connection %client-context
                                                 "tcp" "127.0.0.1"
                                                 %kernel-key
                                                 #:first-port %first-port))
         (server            (connection->kernel connection
                                                #:context %server-context)))
    (define (handle-message kernel kind message state)
      (pk 'handle message)
      (leave-server-loop (message-type message)))

    (define (client-thunk)
      (let ((bv (string->utf8 "Hello, kernel!")))
        (zmq-send-bytevector (kernel-heartbeat client) bv)
        (let ((bv* (zmq-receive-bytevector (kernel-heartbeat client)
                                           (bytevector-length bv))))
          (send-message client
                        (message (header (if (equal? bv* bv)
                                             "success"
                                             "failure")
                                         "foo" "123")
                                 "{}")))))

    (call-with-new-thread client-thunk)
    (let ((result (serve-kernels (list server) handle-message 0)))
      (close-kernel server)
      result)))

(test-end "servers")
