#!/bin/sh
# Guix-kernel -- Guix kernel for Jupyter
# Copyright (C) 2021, 2024 Ludovic Courtès <ludovic.courtes@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Usage: ./pre-inst-env COMMAND ...
#
# Run the given command in an environment where $JUPYTER_PATH points
# to the in-tree Guix-Jupyter kernel.

set -e

# Have 'IPYTHONDIR' point to writable directory to silence a warning from
# IPyKernel (resulting in a "stream" message) about 'HOME' being non-writable
# in Guix chroot builds.
IPYTHONDIR="$(mktemp -d)"
export IPYTHONDIR

kernel_dir="$(mktemp -d)"
trap 'rm -r "$kernel_dir" "$IPYTHONDIR"' EXIT

mkdir -p "$kernel_dir/kernels/guix"
cp "@abs_top_builddir@/kernel.json" "$kernel_dir/kernels/guix"

JUPYTER_PATH="$kernel_dir${JUPYTER_PATH:+:}$JUPYTER_PATH"
GUILE_LOAD_PATH="@abs_top_srcdir@/modules:$GUILE_LOAD_PATH"
GUILE_LOAD_COMPILED_PATH="@abs_top_builddir@/modules:$GUILE_LOAD_COMPILED_PATH"
export JUPYTER_PATH GUILE_LOAD_PATH GUILE_LOAD_COMPILED_PATH

"$@"
